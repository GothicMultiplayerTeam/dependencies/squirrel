# The programming language Squirrel 3.2 stable

## This project has successfully been compiled and run on
 * Windows (x86, x64)
 * Linux (x64, arm, arm64)
 * Web - [Try here online](https://gothicmultiplayerteam.gitlab.io/dependencies/squirrel/repl/index.html)

## Reference
- official documentation: https://gothicmultiplayerteam.gitlab.io/dependencies/squirrel
- electric imp documentation (doesn't include every function): https://developer.electricimp.com/squirrel
- original author: alberto@demichelis.net
- fork maintainer: Patrix